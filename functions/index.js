const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
var db = admin.firestore();
db.settings({ timestampsInSnapshots: true });

const { VisaAPI } = require('./visaapi.js');
const api = new VisaAPI(true);

const uuidv1 = require('uuid/v1');

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

exports.helloWorld = functions.https.onRequest((request, response) => {
    response.send("Hello from Firebase!");
});

exports.exchange = functions.https.onRequest((request, response) => {
    var body = request.body;
    var amount = body.amount;
    var source = body.source;
    var destination = body.destination;
    return api.exchange(amount, source, destination)
        .then((r) => {
            var body = r.data;
            response.send(body);
        })
        .catch((error) => {
            response.status(500).send("Error");
        });
});

exports.documentUpload = functions.https.onRequest((request, response) => {
    var body = request.body;
    var userId = body.userId;
    var imageData = body.imageData;
    var length = imageData.length;
    var bytes = length * 4.0 / 3.0;
    if (bytes < 30*1024 || bytes > 10*1024*1024) {
        response.status(400).send("Image data must be between 30KB and 10MB");
        return;
    }

    var userRef = db.collection('users').doc(userId);
    var uuid = uuidv1();
    var documentRef = userRef.collection('documents').doc(uuid);
    return documentRef.set({ verified: false })
        .then((result) => {
            return api.documentAuth(uuid, imageData);
        })
        .then((visaReferenceId) => {
            console.log("visaReferenceId: " + visaReferenceId);
            if (visaReferenceId === undefined)  {
                response.status(204).send();
                return;
            } else {
                return documentRef.update({ visaReferenceId: visaReferenceId })
                    .then(() => {
                        response.status(204).send();
                    })
                    .catch((error) => {
                        console.error("DB error:", error);
                        response.status(500).send("DB error");
                    });
            }
        })
        .catch((error) => {
            console.error("Error:", error);
            // response.status(500).send("Error");

            // Visa documentverification API is returning 502s incorrect, so skipping errors
            response.status(204).send();
        });
});

exports.documentVerificationCallback = functions.https.onRequest((request, response) => {
    response.status(500).send("This endpoint isn't implemented yet");
});

exports.opportunityUpdated = functions.firestore.document('opportunities/{opportunityId}')
    .onUpdate((change, context) => {
        // Retrieve the current and previous value
        const data = change.after.data();
        const previousData = change.before.data();

        // We'll only update if the name has changed.
        // This is crucial to prevent infinite loops.
        var userRef = data.acceptedUser;
        if (data.acceptedUser === undefined || data.acceptedUser == previousData.acceptedUser) return null;

        // Move to user obj
        // var userId = userRef.id

        console.log("User:", userRef);
        // console.log("User:", userId);
        console.log("Adding data:", data);
        return db.doc(userRef).add(data)
            .then((newRef) => {
                console.log("Added to user");
                // TODO: Send a push notification to the worker
                // TODO: Delete opportunity from root opportunities collection
            })
            .catch((error) => {
                console.error("Error:", error);
            });
    });
