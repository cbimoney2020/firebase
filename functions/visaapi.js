var axios = require('axios');
var https = require('https');
var fs = require('fs');
class VisaAPI {
    constructor(debug) {
        var config = fs.readFileSync("./config/config.json");
        var json = JSON.parse(config);

        this.userId = json.userId;
        this.password = json.password;

        this.certFile = json.certFile;
        this.caFile = json.caFile;
        this.keyFile = json.keyFile;

        this.axios = axios.create({
            timeout: 5000,
            baseURL: "https://sandbox.api.visa.com/",
            httpsAgent: new https.Agent({
              ca: fs.readFileSync(json.caFile),
              cert: fs.readFileSync(json.certFile),
              key: fs.readFileSync(json.keyFile),
              rejectUnauthorized: false
            }),
            headers: {
                'Accept': 'application/json',
                'Authorization' : 'Basic ' + new Buffer(json.userId + ':' + json.password).toString('base64')
            }
        });

        this.axios.interceptors.request.use((config) => {
            return config;
        }, (error) => {
            this.printError(error);
            return Promise.reject(error);
        });
        this.axios.interceptors.response.use((response) => {
            return response;
        }, (error) => {
            this.printError(error);
            return Promise.reject(error);
        });
    }

    // headers is optional
    post(path, body, headers) {
        if (headers === undefined) {
            headers = {}
        }
        return this.axios.post(path, body, { headers: headers });
    }

    exchange(amount, source, destination) {
        var data = {
            destinationCurrencyCode: destination,
            sourceCurrencyCode: source,
            sourceAmount: amount
        };
        var headers = {
            'Content-Type': 'application/json'
        }
        return this.post("forexrates/v1/foreignexchangerates", JSON.stringify(data), headers);
    }

    documentAuth(messageId, imageData) {
        var body = '--VISA_MESSAGE_BOUNDARY\n';
        body += 'Content-Disposition: form-data; name="jsonMessage"\n';
        body += 'Content-Type: application/json; charset=UTF-8\n';
        body += 'Content-Transfer-Encoding: 8bit\n';
        body += JSON.stringify({
            "messageTraceId": messageId
        });
        body += "\n";

        body += '--VISA_MESSAGE_BOUNDARY\n';
        body += 'Content-Disposition: form-data; name="documentFront"; filename="front.jpg"\n';
        body += 'Content-Type: image/jpeg\n';
        body += 'Content-Transfer-Encoding: BASE64\n';
        body += imageData;
        body += '\n';

        body += '--VISA_MESSAGE_BOUNDARY';

        // console.log("Request body: " + body);

        var headers = {
            'Content-Type': 'multipart/form-data;boundary=VISA_MESSAGE_BOUNDARY'
        };
        return this.post("identitydocuments/v1/documentauthentication", body, headers)
            .then((response) => {
                console.log("Response body: " + JSON.stringify(response.data));
                return Promise.resolve(response.data.transactionId);
            });
    }

    printError(error) {
        if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            console.error("Data:", error.response.data);
            console.error("Status:", error.response.status);
            console.error("Headers:", error.response.headers);
        } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            console.error("Request:", error.request);
        } else {
            // Something happened in setting up the request that triggered an Error
            console.error("Error:", error.message);
        }
        console.error("Config:", error.config);
    }
}

module.exports = {
    VisaAPI
}
